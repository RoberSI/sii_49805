// Esfera.cpp: implementation of the Esfera class.
//
//////////////////////////////////////////////////////////////////////

#include "Esfera.h"
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Esfera::Esfera()
{
	crecer=true;
	radio=0.5f;
	velocidad.x=0.5;
	velocidad.y=0.5;
	rojo=245;
	azul=55;
	verde=55;
}

Esfera::~Esfera()
{

}



void Esfera::Dibuja()
{
	glColor3ub(rojo,azul,verde);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}

void Esfera::Mueve(float t)
{
  //To do
	centro.x=centro.x+velocidad.x;
 	centro.y=centro.y+velocidad.y;
 
	if(crecer) radio*=1.005;
	else radio*=0.95;

	if(radio>1.0) crecer=false;
	else if (radio<0.25) crecer=true;

	if(rojo>230 && verde<230 && azul<70) verde+=10;
	else if(rojo>70 && verde>230 && azul<70) rojo-=10;
	else if(rojo<70 && verde>230 && azul<230) azul+=10;
	else if(rojo<70 && verde>70 && azul>230) verde-=10;
	else if(rojo<230 && verde<70 && azul>230) rojo+=10;
	else if(rojo>230 && verde<70 && azul>70) azul-=10;
 
}
